let num1 = prompt('Enter first number: ');
while (!Number(num1) || num1 === '' || num1 % 2 !== 0) {
     if (num1 == '') {
          alert('Empty. Try again!')
     } else {
          if (!Number(num1)) {
               alert('Not a number. Try again!')
          } else {
               if (num1 % 2 !== 0) {
                    alert('Not EVEN number. Try again!')
               }
          }
     }
     num1 = prompt('Enter first number again: ');
}

let num2 = prompt('Enter second number: ');
while (!Number(num2) || num2 === '' || num2 % 2 !== 0) {
     if (num2 == '') {
          alert('Empty. Try again!')
     } else {
          if (!Number(num2)) {
               alert('Not a number. Try again!')
          } else {
               if (num2 % 2 !== 0) {
                    alert('Not EVEN number. Try again!')
               }
          }
     }
     num2 = prompt('Enter second number again: ');
}

if (num1 < num2) {
     for (let i = num1; i <= num2; i++) {
          console.log(i);
     }
} else {
     for (let i = num2; i <= num1; i++) {
          console.log(i);
     }
}